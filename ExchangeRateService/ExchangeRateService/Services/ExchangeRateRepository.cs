﻿using ExchangeRateService.ExternalModels;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ExchangeRateService.Services
{
    public class ExchangeRateRepository : IExchangeRateRepository
    {
        private readonly IConfiguration _configuration;
        private readonly IHttpClientFactory _httpClientFactory;
      
        public ExchangeRateRepository(IConfiguration configuration, IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
            _configuration = configuration;
        }

        public async Task<FixerExchangeRate> GetExchangeRateAysnc()
        {
            FixerExchangeRate fixerExchangeRate;
            string accessKey = _configuration["Fixer:AccessKey"];
            string baseURL = _configuration["Fixer:BaseUrl"];
            using (HttpClient client = _httpClientFactory.CreateClient())
            {
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.GetAsync(String.Format("api/latest?access_key={0}&base={1}", accessKey, "EUR"));
                if (response.IsSuccessStatusCode)
                {
                    fixerExchangeRate = JsonConvert.DeserializeObject<FixerExchangeRate>(await response.Content.ReadAsStringAsync());
                    return fixerExchangeRate;
                }
            }
            return null;
        }
    }
}
