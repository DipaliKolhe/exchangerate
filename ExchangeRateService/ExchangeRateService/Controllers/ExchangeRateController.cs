﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ExchangeRateService.ExternalModels;
using ExchangeRateService.Models;
using ExchangeRateService.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace ExchangeRateService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExchangeRateController : Controller
    {
        private readonly IExchangeRateRepository _exchangeRateRepository;
        private readonly IConfiguration _configuration;

        public ExchangeRateController(IConfiguration configuration, IExchangeRateRepository exchangeRateRepository)
        {
            _exchangeRateRepository = exchangeRateRepository;
            _configuration = configuration;
        }
        // GET api/values
        [HttpGet()]
        public async Task<IActionResult> Get([FromQuery][Required] string baseCurrency, [FromQuery][Required] string targetCurrency)
        {
            if (string.IsNullOrEmpty(baseCurrency) || string.IsNullOrEmpty(targetCurrency))
                return BadRequest(new { error = "Query Parameters baseCurrency and/or targetCurrency missing." });

            baseCurrency = baseCurrency.ToUpper();
            targetCurrency = targetCurrency.ToUpper();         

            List<string> SupportedCurrencies = _configuration.GetSection("SupportedCurrencies:Currency").Get<List<string>>();

            if (SupportedCurrencies.Contains(baseCurrency) && SupportedCurrencies.Contains(targetCurrency))
            {
                ExchangeRate exchangeRate = null;
                if (baseCurrency.Equals(targetCurrency))
                {
                    exchangeRate = new ExchangeRate
                    {
                        baseCurrency = baseCurrency,
                        targetCurrency = targetCurrency,
                        timestamp = DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss'Z'"),
                        exchangeRate = 1M
                    };
                }
                else
                {
                    FixerExchangeRate fixerExchangeRate = await _exchangeRateRepository.GetExchangeRateAysnc();
                    if (fixerExchangeRate != null)
                    {
                        DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(fixerExchangeRate.timestamp);
                        if (fixerExchangeRate.success)
                        {
                            exchangeRate = new ExchangeRate
                            {
                                baseCurrency = baseCurrency,
                                targetCurrency = targetCurrency,
                                timestamp = dateTimeOffset.LocalDateTime.ToString("yyyy-MM-dd'T'HH:mm:ss'Z'"),
                                exchangeRate = decimal.Round(baseCurrency == "EUR" ? fixerExchangeRate.rates[targetCurrency] : fixerExchangeRate.rates[targetCurrency] / fixerExchangeRate.rates[baseCurrency], 5, MidpointRounding.AwayFromZero)
                            };
                        }
                    }
                }
                return Ok(exchangeRate);
            }
            else
            {
                return BadRequest(new { error = "Unsupported base and/or target Currency." });
            }
        }
    }    
}
