﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExchangeRateService.ExternalModels
{
    public class FixerExchangeRate
    {
        public bool success { get; set; }
        [JsonProperty("base")]
        public string baseCurrency { get; set; }
        public string date { get; set; }
        public long timestamp { get; set; }
        public Dictionary<string, decimal> rates { get; set; }
    }
}
