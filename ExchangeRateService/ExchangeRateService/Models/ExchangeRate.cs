﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExchangeRateService.Models
{
    public class ExchangeRate
    {
        public string baseCurrency { get; set; }
        public string targetCurrency { get; set; }
        public decimal exchangeRate { get; set; }
        public string timestamp { get; set; }
    }
}
