using ExchangeRateService;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace ExchangeRateIntegrationTest
{
    public class ExchangeRateIntegrationTest : IClassFixture<TestFixture<Startup>>
    {
        private HttpClient Client;

        public ExchangeRateIntegrationTest(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
        }

        [Fact]
        public async Task TestGetExchangeRateAsync()
        {
            // Arrange
            var request = new
            {
                Url = "api/ExchangeRate?baseCurrency=USD&targetCurrency=AUD"
            };

            // Act
            var response = await Client.GetAsync(request.Url);

            // Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestGetExchangeRateAsyncWithUnsupportedCurrencies()
        {
            // Arrange
            var request = new
            {
                Url = "api/ExchangeRate?baseCurrency=USD&targetCurrency=INR"
            };

            // Act
            var response = await Client.GetAsync(request.Url);

            // Assert
            response.StatusCode.Equals(HttpStatusCode.BadRequest);
        }
        [Fact]
        public async Task TestGetExchangeRateAsyncWithNoQueryStringParameters()
        {
            // Arrange
            var request = new
            {
                Url = "api/ExchangeRate"
            };

            // Act
            var response = await Client.GetAsync(request.Url);

            // Assert
            response.StatusCode.Equals(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task TestGetExchangeRateAsyncWithEmptyQueryStringParameters()
        {
            // Arrange
            var request = new
            {
                Url = "api/ExchangeRate?baseCurrency=&targetCurrency="
            };

            // Act
            var response = await Client.GetAsync(request.Url);

            // Assert
            response.StatusCode.Equals(HttpStatusCode.BadRequest);
        }

    }
}
