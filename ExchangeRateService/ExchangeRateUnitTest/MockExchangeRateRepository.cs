﻿using ExchangeRateService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using ExchangeRateService.ExternalModels;
using ExchangeRateService.Services;

namespace ExchangeRateUnitTest
{
    public class MockExchangeRateRepository : IExchangeRateRepository
    {
        private FixerExchangeRate _fixerExchangeRate;
        public MockExchangeRateRepository()
        {
            _fixerExchangeRate = new FixerExchangeRate
            {

            };
        }
        public async Task<FixerExchangeRate> GetExchangeRateAysnc()
        {
            _fixerExchangeRate = new FixerExchangeRate
            {
                baseCurrency = "EUR",
                date = DateTime.Now.ToString("yyyy-MM-dd"),
                success = true,
                rates = new Dictionary<string, decimal>(),
                timestamp = ((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds()
            };


            await Task.Run(() => addItems());

            return _fixerExchangeRate;
        }

        private void addItems()
        {
            _fixerExchangeRate.rates.Add("AUD", 1.596504M);
            _fixerExchangeRate.rates.Add("SEK", 10.477752M);
            _fixerExchangeRate.rates.Add("USD", 1.130026M);
            _fixerExchangeRate.rates.Add("GBP", 0.8564M);
            _fixerExchangeRate.rates.Add("EUR", 1M);
        }
    }
}
