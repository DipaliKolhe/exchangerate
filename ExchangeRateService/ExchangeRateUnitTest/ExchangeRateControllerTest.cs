using ExchangeRateService.Controllers;
using ExchangeRateService.Models;
using ExchangeRateService.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using Xunit;

namespace ExchangeRateUnitTest
{
    public class ExchangeRateControllerTest
    {
        ExchangeRateController _exchangeRateController;
        IExchangeRateRepository _exchangeRateRepository;
        IConfiguration _configuration;

        public ExchangeRateControllerTest()
        {
            _exchangeRateRepository = new MockExchangeRateRepository();
            _configuration = new ConfigurationBuilder()
                            .AddJsonFile("appsettings.json")
                            .Build();
            _exchangeRateController = new ExchangeRateController(_configuration, _exchangeRateRepository);
        }

        [Fact]
        public void ExchangeRateControllerValidCurrencyTestBaseEUR()
        {
            //Arrange
            string baseCurrency = "EUR";
            string targetCurrency = "USD";

            //Act
            var result = _exchangeRateController.Get(baseCurrency, targetCurrency).Result as IActionResult;

            //Assert
            Assert.IsType<OkObjectResult>(result);
            Assert.IsType<ExchangeRate>(((OkObjectResult)result).Value);
        }

        [Fact]
        public void ExchangeRateControllerValidCurrencyTestAnyBase()
        {
            //Arrange
            string baseCurrency = "AUD";
            string targetCurrency = "USD";

            //Act
            var result = _exchangeRateController.Get(baseCurrency, targetCurrency).Result as IActionResult;

            //Assert
            Assert.IsType<OkObjectResult>(result);
            Assert.IsType<ExchangeRate>(((OkObjectResult)result).Value);

        }

        [Fact]
        public void ExchangeRateControllerValidCurrencyTestLowerCase()
        {
            //Arrange
            string baseCurrency = "aud";
            string targetCurrency = "usd";

            //Act
            var result = _exchangeRateController.Get(baseCurrency, targetCurrency).Result as IActionResult;

            //Assert
            Assert.IsType<OkObjectResult>(result);
            Assert.IsType<ExchangeRate>(((OkObjectResult)result).Value);

        }

        [Fact]
        public void ExchangeRateControllerUnsupportedCurrencies()
        {
            //Arrange
            string baseCurrency = "AUD";
            string targetCurrency = "INR";

            //Act
            var result = _exchangeRateController.Get(baseCurrency, targetCurrency).Result as BadRequestObjectResult;

            //Assert           
            Assert.Contains("Unsupported base and/or target Currency.", result.Value.ToString());
        }

        [Fact]
        public void ExchangeRateControllerCurrenciesNotProvided()
        {           

            //Act
            var result = _exchangeRateController.Get(null, null).Result as BadRequestObjectResult;

            //Assert
            Assert.Contains("Query Parameters baseCurrency and/or targetCurrency missing.", result.Value.ToString());
        }

        [Fact]
        public void ExchangeRateControllerBaseEqualsTarget()
        {
            //Arrange
            string baseCurrency = "AUD";
            string targetCurrency = "AUD";

            //Act
            var result = _exchangeRateController.Get(baseCurrency, targetCurrency).Result as OkObjectResult;

            //Assert
            Assert.IsType<ExchangeRate>(result.Value);
        }
    }
}
