﻿using ExchangeRateService.ExternalModels;
using ExchangeRateService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExchangeRateService.Services
{
    public interface IExchangeRateRepository
    {
        Task<FixerExchangeRate> GetExchangeRateAysnc();
    }
}